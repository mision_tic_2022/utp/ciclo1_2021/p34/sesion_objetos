
#Importar la clase Carro que se encuentra en el fichero carro.py
from carro import Carro

#Clase que representa a una persona
class Persona:
    #Método constructor
    def __init__(self, nombre: str, apellido: str):
        #Atributos
        self.nombre = nombre
        self.apellido = apellido
        self.carros: list[Carro] = []
    
    """
    Métodos consultores
    """
    def get_nombre(self):
        return self.nombre

    def get_apellido(self):
        return self.apellido

    def get_carros(self)->list:
        return self.carros
    
    """
    Métodos modificadores
    """
    def set_nombre(self, nombre):
        self.nombre = nombre
    
    def set_apellido(self, apellido):
        self.apellido = apellido
    
    def construir_carro(self, color: str, marca: str, modelo: str)->list:
        carro: Carro = Carro(color, marca, modelo)
        self.carros.append(carro)
        return carro
    


#Crea un objeto de tipo Persona
obj_persona: Persona = Persona("Juan", "Smith")
print(obj_persona.get_nombre())
print(obj_persona.get_apellido())
print("--------------------------------")
#Modificamos los valores del objeto
obj_persona.set_nombre("Pedro")
obj_persona.set_apellido("Perez")
print(obj_persona.get_nombre())
print(obj_persona.get_apellido())
#Hacemos que la persona construya un carro
#carro = obj_persona.construir_carro()
#print("-------Carro Construido--------------")
#print(carro)

"""
Requerimiento:
Actualizar el método 'construir_Carro()' para 
que construya carros a partir de datos específicos y 
estos carros almacenarlos en una lista.
Al final imprimir todos los carros que construyó la persona.
"""
print("-----------Construcción de carros-----------------")
carro1 = obj_persona.construir_carro('blanco', 'mazda', '2105')
carro1 = obj_persona.construir_carro('blanco', 'mazda', '2005')
carro2 = obj_persona.construir_carro('negro', 'mazda', '1997')


print(obj_persona.get_carros())

for carro in obj_persona.get_carros():
    print(carro)



"""
Repositorio:
https://gitlab.com/mision_tic_2022/utp/ciclo1_2021/p34
"""
