#Clase que representa a un carro
class Carro:
    #Mètodo constructor
    def __init__(self, color: str, marca: str, modelo: str) -> None:
        self.color = color
        self.marca = marca
        self.modelo = modelo

    #Convierte el objeto a string al momento de imprimirlo
    def __str__(self) -> str:
        str_carro: str = "Color: "+self.color+"\n"
        str_carro += "Marca: "+self.marca+"\n"
        str_carro += "Modelo: "+self.modelo+"\n"
        return str_carro

    """
    Métodos consultores
    """
    def get_color(self)->str:
        return self.color
    
    def get_marca(self)->str:
        return self.marca
    
    def get_modelo(self)->str:
        return self.modelo
    
    """
    Métodos modificadores
    """
    def set_color(self, color: str):
        self.color = color
    
    def set_marca(self, marca: str):
        self.marca = marca
    
    def set_modelo(self, modelo: str):
        self.modelo = modelo
